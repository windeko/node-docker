require('dotenv').config()

const express = require('express')

const app = express()

app.get('', (req, res) => (res.send(
    'Welcome, stranger'
)))

// ERROR HANDLING
app.use(function (err, req, res, next) {
    if (!err.status) err.status = 500 // If err has no specified error code, set error code to 'Internal Server Error (500)'
    res.status(err.status).send({ error: err.message }) // All HTTP requests must have a response, so let's send back an error with its status code and message
})

const port = process.env.PORT || 3000
app.listen(port, () => console.log('Express starts on', port, 'port'))
